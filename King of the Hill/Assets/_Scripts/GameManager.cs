﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public Transform
        WhitePlayer,
        BlackPlayer;
    
    public List<Transform>
        WhitePlayerPieces,
        BlackPlayerPieces;

    private bool WhiteTurn = true;
    
    private int
        //Values: 20, 16, 12, 8, 4
        _reductionTurn = 20,
        _currentTurn = 0;

    public Text TurnCount;

    public GameObject[]
        WhitePieceType,
        BlackPieceType;

    private void Awake()
    {
        WhitePlayer = GameObject.FindGameObjectWithTag("WhitePlayer").transform;
        BlackPlayer = GameObject.FindGameObjectWithTag("BlackPlayer").transform;

        UpdateTurnCount();
    }

    // Start is called before the first frame update
    IEnumerator Start()
    {
        for (int i = 0; i < WhitePlayer.childCount; i++)
        {
            WhitePlayerPieces.Add(WhitePlayer.GetChild(i));
        }
        
        for (int i = 0; i < BlackPlayer.childCount; i++)
        {
            BlackPlayerPieces.Add(BlackPlayer.GetChild(i));
        }

        yield return new WaitForEndOfFrame();

        WhitePlayersTurn();
    }

    private void Update()
    {
        if ((Input.touchCount > 0) && (Input.GetTouch(0).phase == TouchPhase.Began))
        {
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.GetTouch(0).position), out hit))
            {
                hit.transform.gameObject.SendMessage("Interact", SendMessageOptions.DontRequireReceiver);
            }
        }
    }

    public void SwitchTurn()
    {
        _currentTurn++;
        Debug.Log(_currentTurn);
        CheckTurn();

        WhiteTurn = !WhiteTurn;

        if(WhiteTurn)
        {
            WhitePlayersTurn();
        }
        else
        {
            BlackPlayersTurn();
        }
    }

    private void CheckTurn()
    {
        if(_currentTurn == _reductionTurn)
        {
            if(transform.childCount > 0)
            {
                transform.GetChild(0).GetComponent<ReductionBehaviour>().Destroy();
            }

            _currentTurn = 0;
            _reductionTurn -= 4;
        }

        if(_reductionTurn > 0)
        {
            UpdateTurnCount();
        }
        else
        {
            TurnCount.text = "";
        }
    }

    private void UpdateTurnCount()
    {
        TurnCount.text = (_reductionTurn - _currentTurn).ToString();
    }

    private void WhitePlayersTurn()
    {
        StartCoroutine(ActivateWhitePieces());
    }

    private IEnumerator ActivateWhitePieces()
    {
        yield return new WaitForSeconds(0.05f);
        foreach (Transform blackPiece in BlackPlayerPieces)
        {
            blackPiece.GetComponent<GenericPiece>().Interactable = false;
            blackPiece.GetComponents<BoxCollider>()[0].enabled = false;
            blackPiece.GetComponents<BoxCollider>()[1].enabled = false;
        }

        foreach (Transform whitePiece in WhitePlayerPieces)
        {
            whitePiece.GetComponent<GenericPiece>().Interactable = true;
            whitePiece.GetComponents<BoxCollider>()[0].enabled = true;
            whitePiece.GetComponents<BoxCollider>()[1].enabled = true;
        }
    }

    private void BlackPlayersTurn()
    {
        StartCoroutine(ActivateBlackPieces());
    }

    private IEnumerator ActivateBlackPieces()
    {
        yield return new WaitForSeconds(0.05f);
        foreach (Transform whitePiece in WhitePlayerPieces)
        {
            whitePiece.GetComponent<GenericPiece>().Interactable = false;
            whitePiece.GetComponents<BoxCollider>()[0].enabled = false;
            whitePiece.GetComponents<BoxCollider>()[1].enabled = false;
        }

        foreach (Transform blackPiece in BlackPlayerPieces)
        {
            blackPiece.GetComponent<GenericPiece>().Interactable = true;
            blackPiece.GetComponents<BoxCollider>()[0].enabled = true;
            blackPiece.GetComponents<BoxCollider>()[1].enabled = true;
        }
    }
}
