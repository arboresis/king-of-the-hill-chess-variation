﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReductionBehaviour : MonoBehaviour
{
    private MoveCheck _moveCheck;

    private GameManager _gameManager;

    // Start is called before the first frame update
    void Awake()
    {
        _moveCheck = transform.parent.GetComponent<MoveCheck>();
        _gameManager = transform.parent.GetComponent<GameManager>();
    }

    public void Destroy()
    {
        StartCoroutine(DestroyLines());
    }

    private IEnumerator DestroyLines()
    {
        yield return new WaitForSeconds(0.05f);
        for (int i = 0; i < transform.childCount; i++)
        {
            _moveCheck.BoardSquares.Remove(transform.GetChild(i));
            if (transform.GetChild(i).GetComponent<OccupationCheck>().OccupyingPiece != null)
            {
                Transform piece = transform.GetChild(i).GetComponent<OccupationCheck>().OccupyingPiece;
                if (_gameManager.WhitePlayerPieces.Contains(piece))
                {
                    _gameManager.WhitePlayerPieces.Remove(piece);
                }

                if (_gameManager.BlackPlayerPieces.Contains(piece))
                {
                    _gameManager.BlackPlayerPieces.Remove(piece);
                }

                Destroy(piece.gameObject);
            }
        }

        Destroy(gameObject);
    }
}
