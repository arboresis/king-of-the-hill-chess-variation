﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapReductionManager : MonoBehaviour
{
    public Transform[] ReductionParents;

    [SerializeField]
    private List<GameObject>
        _allSquares,
        _firstReductionSquares,
        _secondReductionSquares,
        _thirdReductionSquares,
        _fourthReductionSquares,
        _fifthReductionSquares;
    
    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return new WaitForEndOfFrame();
        foreach (GameObject whiteSquare in GameObject.FindGameObjectsWithTag("WhiteSquare"))
        {
            _allSquares.Add(whiteSquare);
        }

        foreach (GameObject blackSquare in GameObject.FindGameObjectsWithTag("BlackSquare"))
        {
            _allSquares.Add(blackSquare);
        }

        GetVerticalSquares(_firstReductionSquares, 1);
        GetHorizontalSquares(_secondReductionSquares, 2);
        GetVerticalSquares(_thirdReductionSquares, 3);
        GetHorizontalSquares(_fourthReductionSquares, 4);
        GetVerticalSquares(_fifthReductionSquares, 5);
    }

    private void GetVerticalSquares(List<GameObject> verticalSquareList, int reductionIndex)
    {
        foreach(GameObject square in _allSquares.ToArray())
        {
            /*
             * 1    -   1, 2, 15, 16
             * 3    -   3, 4, 13, 14
             * 5    -   5, 6, 11, 12
             */

            int
                minZ1 = reductionIndex,
                minZ2 = reductionIndex + 1,
                maxZ1 = 17 - reductionIndex,
                maxZ2 = 17 - reductionIndex - 1;

            if (square.transform.position.z == minZ1)
            {
                AddSquare(square);
            }
            if (square.transform.position.z == minZ2)
            {
                AddSquare(square);
            }
            if (square.transform.position.z == maxZ1)
            {
                AddSquare(square);
            }
            if (square.transform.position.z == maxZ2)
            {
                AddSquare(square);
            }
        }

        void AddSquare(GameObject square)
        {
            verticalSquareList.Add(square);
            square.transform.SetParent(ReductionParents[reductionIndex - 1]);
            _allSquares.Remove(square);
        }
    }

    private void GetHorizontalSquares(List<GameObject> horizontalSquareList, int reductionIndex)
    {
        foreach (GameObject square in _allSquares.ToArray())
        {
            /*
             * 2    -   1, 8
             * 4    -   2, 7
             */

            int
                minX = reductionIndex / 2,
                maxX = 9 - reductionIndex / 2;

            if (square.transform.position.x == minX)
            {
                AddSquare(square);
            }
            if (square.transform.position.x == maxX)
            {
                AddSquare(square);
            }
        }

        void AddSquare(GameObject square)
        {
            horizontalSquareList.Add(square);
            square.transform.SetParent(ReductionParents[reductionIndex - 1]);
            _allSquares.Remove(square);
        }
    }
}
