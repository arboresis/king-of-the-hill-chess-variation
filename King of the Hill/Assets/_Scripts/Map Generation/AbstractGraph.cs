﻿using UnityEngine;

public class AbstractGraph : MonoBehaviour
{
    public int
        _verticalDimensions,
        _horizontalDimensions;

    public AbstractNode[,] Nodes;

    public void GraphInitialization(int[,] terrainMap)
    {
        _verticalDimensions = terrainMap.GetLength(1);
        _horizontalDimensions = terrainMap.GetLength(0);

        Nodes = new AbstractNode[_horizontalDimensions, _verticalDimensions];

        for (int y = 0; y < _verticalDimensions; y++)
        {
            for (int x = 0; x < _horizontalDimensions; x++)
            {
                NodeType type = (NodeType) terrainMap[x, y];
                AbstractNode newNode = new AbstractNode(x, y, type);
                newNode.NodePosition = new Vector3(x, 0, y);
                Nodes[x, y] = newNode;
            }
        }
    }

    public AbstractNode GetNode(int x, int y)
    {
        return Nodes[x, y];
    }

    public AbstractNode GetNextNode(AbstractNode Node)
    {
        return Nodes[Node.x + 1, Node.y];
    }

    public AbstractNode GetPreviousNode(AbstractNode Node)
    {
        return Nodes[Node.x - 1, Node.y];
    }

    public AbstractNode GetNextYNode(AbstractNode Node)
    {
        return Nodes[Node.x, Node.y + 1];
    }

    public AbstractNode GetPreviousYNode(AbstractNode Node)
    {
        return Nodes[Node.x, Node.y - 1];
    }
}
