﻿using UnityEngine;

public class NodeObject : MonoBehaviour
{
    public GameObject Prefab;

    public void Init(AbstractNode node)
    {
        transform.position = node.NodePosition;
        Prefab.transform.localScale = new Vector3(1f, 1f, 1f);
    }
}
