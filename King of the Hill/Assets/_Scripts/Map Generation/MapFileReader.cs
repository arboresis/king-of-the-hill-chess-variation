﻿using System.Collections.Generic;
using UnityEngine;

public class MapFileReader : MonoBehaviour
{
    //The map's width and height
    public int
        MAP_WIDTH = 10,
        MAP_HEIGHT = 2;

    //The file from which the text will be read in order to build the map
    public TextAsset TextFile;
   
    //The Resources/TerrainMapFiles folder's location
    public string ResourcePath = "TerrainMapFiles";

    //A public method to prevent direct access
    public List<string> ReadFileLines()
    {
        return ReadFileLines(TextFile);
    }

    private List<string> ReadFileLines(TextAsset TextFile)
    {
        //Create a new string list that will take the file's text lines
        List<string> FileLines = new List<string>();

        //Put all the text into a single string
        string LineText = TextFile.text;

        //Split the full text string into various pieces through paragraph recognition
        string[] Delimiters = { "\r\n", "\n" };
        FileLines.AddRange(LineText.Split(Delimiters, System.StringSplitOptions.None));
        //Revert the list because the text lines will be read in reverse
        FileLines.Reverse();

        return FileLines;
    }

    public void ChangeMapDimensions(List<string> FileTextLines)
    {
        //Set the map height as the number of horizontal lines in the list
        MAP_HEIGHT = FileTextLines.Count;

        //Set the map width with normalization if part of the line is omitted
        foreach (string Line in FileTextLines)
        {
            int LineSize = Line.Length;

            if (LineSize > MAP_WIDTH)
            {
                MAP_WIDTH = LineSize;
            }
        }
    }

    public int[,] FileToArray()
    {
        List<string> FileTextLines  = new List<string>();

        FileTextLines = ReadFileLines(TextFile);
        ChangeMapDimensions(FileTextLines);

        int[,] MapArray = new int[MAP_WIDTH, MAP_HEIGHT];

        for (int y = 0; y < MAP_HEIGHT; y++)
        {
            for (int x = 0; x < MAP_WIDTH; x++)
            {
                if (x < FileTextLines[y].Length)
                {
                    char c = FileTextLines[y][x];
                    MapArray[x, y] = (int) char.GetNumericValue(c);
                }
            }
        }
        return MapArray;
    }
}
