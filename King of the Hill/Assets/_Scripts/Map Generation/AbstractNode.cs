﻿using UnityEngine;

public enum NodeType
{
    Type0 = 0,
    Type1 = 1,
    Type2 = 2,
    Type3 = 3,
    Type4 = 4,
    Type5 = 5,
    Type6 = 6,
    Type7 = 7,
    Type8 = 8,
    Type9 = 9
};

public class AbstractNode
{
    //Set the default type to type 0
    public NodeType type = NodeType.Type0;

    //Set the default position values
    public int
        x = -1,
        y = -1;

    //The node's position in three-dimensional space
    public Vector3 NodePosition;

    public AbstractNode(int xIndex, int yIndex, NodeType nodeType)
    {
        x = xIndex;
        y = yIndex;
        type = nodeType;
    }
}
