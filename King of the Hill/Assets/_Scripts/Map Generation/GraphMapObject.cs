﻿using UnityEngine;

[RequireComponent(typeof(AbstractGraph))]
public class GraphMapObject : MonoBehaviour
{
    public GameObject[] NodePrefabs = new GameObject[7];

    public void Initialization(AbstractGraph graph)
    {
        foreach (AbstractNode n in graph.Nodes)
        {
            GameObject instance;

            switch (n.type)
            {
                case NodeType.Type0:
                    break;
                case NodeType.Type1:
                    instance = Instantiate(NodePrefabs[0], n.NodePosition, Quaternion.identity);
                    instance.transform.SetParent(GameObject.FindGameObjectWithTag("BoardTerrain").transform);
                    break;
                case NodeType.Type2:
                    instance = Instantiate(NodePrefabs[1], n.NodePosition, Quaternion.identity);
                    instance.transform.SetParent(GameObject.FindGameObjectWithTag("BoardTerrain").transform);
                    break;
                case NodeType.Type3:
                    instance = Instantiate(NodePrefabs[2], n.NodePosition, Quaternion.identity);
                    instance.transform.SetParent(GameObject.FindGameObjectWithTag("BoardTerrain").transform);
                    break;
                case NodeType.Type4:
                    instance = Instantiate(NodePrefabs[3], n.NodePosition, Quaternion.identity);
                    instance.transform.SetParent(GameObject.FindGameObjectWithTag("BoardTerrain").transform);
                    break;
                case NodeType.Type5:
                    instance = Instantiate(NodePrefabs[4], n.NodePosition, Quaternion.identity);
                    instance.transform.SetParent(GameObject.FindGameObjectWithTag("BoardTerrain").transform);
                    break;
                case NodeType.Type6:
                    instance = Instantiate(NodePrefabs[5], n.NodePosition, Quaternion.identity);
                    instance.transform.SetParent(GameObject.FindGameObjectWithTag("BoardTerrain").transform);
                    break;
                case NodeType.Type7:
                    instance = Instantiate(NodePrefabs[6], n.NodePosition, Quaternion.identity);
                    instance.transform.SetParent(GameObject.FindGameObjectWithTag("BoardTerrain").transform);
                    break;
                case NodeType.Type8:
                    instance = Instantiate(NodePrefabs[7], n.NodePosition, Quaternion.identity);
                    instance.transform.SetParent(GameObject.FindGameObjectWithTag("BoardTerrain").transform);
                    break;
                case NodeType.Type9:
                    instance = Instantiate(NodePrefabs[8], n.NodePosition, Quaternion.identity);
                    instance.transform.SetParent(GameObject.FindGameObjectWithTag("BoardTerrain").transform);
                    break;
            }
        }
    }
}



