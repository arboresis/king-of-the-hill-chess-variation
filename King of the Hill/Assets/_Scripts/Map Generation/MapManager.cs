﻿using UnityEngine;

public class MapManager : MonoBehaviour
{
    private MapFileReader TerrainMap;
    private AbstractGraph Graph;

    // Start is called before the first frame update
    void Start()
    {
        TerrainMap = GameObject.FindGameObjectWithTag("BoardMap").GetComponent<MapFileReader>();
        Graph = GameObject.FindGameObjectWithTag("GraphContainer").GetComponent<AbstractGraph>();

        int[,] Map = TerrainMap.FileToArray();
        Graph.GraphInitialization(Map);

        GraphMapObject graphMapObject = Graph.gameObject.GetComponent<GraphMapObject>();
        graphMapObject.Initialization(Graph);
    }
}
