﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackPawn : GenericPiece
{
    // Update is called once per frame
    new void Update()
    {
        RaycastHit
            leftHit,
            rightHit;

        Physics.Raycast(transform.position - new Vector3(0, 1, 0), new Vector3(PieceDirections[0].x + 1, 0, PieceDirections[0].y), out leftHit, 2 * Mathf.Sqrt(2));
        Physics.Raycast(transform.position - new Vector3(0, 1, 0), new Vector3(PieceDirections[0].x - 1, 0, PieceDirections[0].y), out rightHit, 2 * Mathf.Sqrt(2));

        if (rightHit.collider.CompareTag("WhiteSquare") || rightHit.collider.CompareTag("BlackSquare"))
        {
            if (rightHit.collider.GetComponent<OccupationCheck>().IsOccupied && rightHit.collider.GetComponent<OccupationCheck>().OccupyingPieceIsWhite)
            {
                Debug.Log("Pawn can move");
                PieceDirections[1] = new Vector2(-1, -1);
            }
            else
            {
                PieceDirections[1] = new Vector2(0, 0);
            }
        }

        if (leftHit.collider.CompareTag("WhiteSquare") || leftHit.collider.CompareTag("BlackSquare"))
        {
            if (leftHit.collider.GetComponent<OccupationCheck>().IsOccupied && leftHit.collider.GetComponent<OccupationCheck>().OccupyingPieceIsWhite)
            {
                Debug.Log("Pawn can move");
                PieceDirections[2] = new Vector2(1, -1);
            }
            else
            {
                PieceDirections[2] = new Vector2(0, 0);
            }
        }

        foreach (Vector2 direction in PieceDirections)
        {
            RaycastHit hit;
            if (ShouldIterate)
            {
                if (Physics.Raycast(transform.position, new Vector3(direction.x, 0f, direction.y), out hit, 16f))
                {
                    if (hit.collider.name.Contains("King") && hit.collider.name.Contains("White"))
                    {
                        Debug.Log("Check");
                    }
                }
            }
            else
            {
                Debug.DrawRay(transform.position - new Vector3(0, 1, 0), new Vector3(direction.x, 0f, direction.y));
                if (Physics.Raycast(transform.position, new Vector3(direction.x, 0f, direction.y), out hit, direction.magnitude))
                {
                    if (hit.collider.name.Contains("King") && hit.collider.name.Contains("White"))
                    {
                        Debug.Log("Check");
                    }
                }
            }
        }
    }
}
