﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KingPiece : GenericPiece
{
    new protected bool CheckIfCanMoveToSquare(bool isOccupied, bool occupyingIsWhite)
    {
        //If the square isn't occupied, the king piece *may* be able to move there
        if (!isOccupied)
        {
            //return true;
            //Checks if the movement would set the king to check
            //if(Physics.)
        }

        //If the occupying piece is white and this piece is black, it can destroy
        if (occupyingIsWhite)
        {
            if (tag == "BlackPiece")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //If the occupying piece is black and this piece is white, it can destroy
        else
        {
            if (tag == "WhitePiece")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
