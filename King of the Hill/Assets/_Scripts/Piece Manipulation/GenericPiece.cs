﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericPiece : MonoBehaviour
{
    //The list of directions in which this piece can move
    public List<Vector2> PieceDirections = new List<Vector2>();

    //The script that checks the move and lights up the squares
    protected MoveCheck _moveCheck;

    //Check if the move check should be iterative
    public bool ShouldIterate = true;

    //Check if this piece is interactable
    public bool Interactable = false;

    protected void Awake()
    {
        //Instance creation
        _moveCheck = GameObject.FindGameObjectWithTag("MoveCheck").GetComponent<MoveCheck>();
    }

    protected void Update()
    {
        foreach (Vector2 direction in PieceDirections)
        {
            RaycastHit hit;
            if(Physics.Raycast(transform.position, direction, out hit))
            {
                if(hit.collider.gameObject != gameObject && hit.collider.name.Contains("King"))
                {
                    Debug.Log("Check");
                }
            }
        }
    }

    public void CheckMovement()
    {
        //Check each direction in which the piece can move
        foreach(Vector2 direction in PieceDirections)
        {
            //Light up the possible squares
            LightUpMovement(transform.position, direction);
        }
    }

    protected void LightUpMovement(Vector3 origin, Vector2 direction)
    {
        //The vector used to increment (if the square search is iterative)
        Vector2 incrementVector;
        //The target square
        Transform targetSquare = _moveCheck.GetSquare(origin + new Vector3(direction.x, -1, direction.y));

        //Check if the target square exists
        if (targetSquare != null)
        {
            //The occupation check for the target square
            OccupationCheck targetSquareOccupation = targetSquare.GetComponent<OccupationCheck>();

            //Check if the target square is occupied by a piece (and get the piece's side) and determine whether this piece can move there
            if (CheckIfCanMoveToSquare(
                                        targetSquareOccupation.IsOccupied,
                                        targetSquareOccupation.OccupyingPieceIsWhite))
            {
                //Light up the possible square according to the current direction check
                _moveCheck.LightUpSquare(targetSquare.position);

                //Check if the square search should be iterative
                if (ShouldIterate)
                {
                    //Check if the target square is not occupied
                    if (!targetSquareOccupation.IsOccupied)
                    {
                        //Check if the direction absolute values for xx and yy are the same (check if the current direction is diagonal)
                        if(Mathf.Abs(direction.x) != Mathf.Abs(direction.y))
                        {
                            //If the direction is not diagonal, the next square search will be based on a vector with length 1
                            incrementVector = direction + direction.normalized;
                        }
                        else
                        {
                            //If the direction is not diagonal, the next square search will be based on a vector with length 2/√2
                            incrementVector = direction + (direction.normalized * (2 / Mathf.Sqrt(2)));
                        }
                        //Search for the next square in this direction
                        LightUpMovement(origin, incrementVector);
                    }
                }
            }
        }
    }

#if UNITY_ANDROID
    protected void Interact()
    {
        Debug.Log("Piece selection came from Android!");
        if (Interactable)
        {
            if (_moveCheck.ActivePiece != transform)
            {
                _moveCheck.ActivePiece = transform;
                _moveCheck.ResetSquareHighlighters();
                CheckMovement();
            }
            else
            {
                _moveCheck.ActivePiece = null;
                _moveCheck.ResetSquareHighlighters();
            }
        }
    }
#endif

#if UNITY_EDITOR
    protected void OnMouseDown()
    {
        Debug.Log("Piece selection came from Unity Editor!");
        if (Interactable)
        {
            if (_moveCheck.ActivePiece != transform)
            {
                _moveCheck.ActivePiece = transform;
                _moveCheck.ResetSquareHighlighters();
                CheckMovement();
            }
            else
            {
                _moveCheck.ActivePiece = null;
                _moveCheck.ResetSquareHighlighters();
            }
        }
    }
#endif

    protected bool CheckIfCanMoveToSquare(bool isOccupied, bool occupyingIsWhite)
    {
        //If the square isn't occupied, the piece can move there
        if(!isOccupied)
        {
            return true;
        }

        //If the occupying piece is white and this piece is black, it can destroy
        if(occupyingIsWhite)
        {
            if(tag == "BlackPiece")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //If the occupying piece is black and this piece is white, it can destroy
        else
        {
            if (tag == "WhitePiece")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
