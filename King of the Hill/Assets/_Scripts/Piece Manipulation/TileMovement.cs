﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileMovement : MonoBehaviour
{
    //The script that holds the active piece
    private MoveCheck _moveCheck;

    //The script that handles the player turns
    private GameManager _gm;

    //The parent's occupation check
    private OccupationCheck _occupationCheck;
    
    void Awake()
    {
        //Instance creation
        _moveCheck = GameObject.FindGameObjectWithTag("MoveCheck").GetComponent<MoveCheck>();
        _gm = GameObject.FindGameObjectWithTag("MoveCheck").GetComponent<GameManager>();
        _occupationCheck = transform.parent.GetComponent<OccupationCheck>();

        GetComponent<SpriteRenderer>().color = Color.blue;
    }

#if UNITY_ANDROID
    private void Interact()
    {
        Debug.Log("Tile selection came from Android!");
        if (_moveCheck.ActivePiece != null)
        {
            if (_occupationCheck.OccupyingPiece != null)
            {
                if(_occupationCheck.OccupyingPiece.CompareTag("WhitePiece"))
                {
                    _gm.WhitePlayerPieces.Remove(_occupationCheck.OccupyingPiece);
                }
                else if(_occupationCheck.OccupyingPiece.CompareTag("BlackPiece"))
                {
                    _gm.BlackPlayerPieces.Remove(_occupationCheck.OccupyingPiece);
                }

                Destroy(_occupationCheck.OccupyingPiece.gameObject);
                
                _occupationCheck.ResetOccupationSpecs();

                PieceDiceRoll(_moveCheck.ActivePiece);
            }

            _moveCheck.ActivePiece.position = new Vector3(transform.position.x, _moveCheck.ActivePiece.position.y, transform.position.z);
        }

        GetComponent<SpriteRenderer>().color = Color.blue;

        _moveCheck.ActivePiece = null;
        _moveCheck.ResetSquareHighlighters();

        _gm.SwitchTurn();
    }
#endif

#if UNITY_EDITOR
    private void OnMouseDown()
    {
        Debug.Log("Tile selection came from Unity Editor!");
        if (_moveCheck.ActivePiece != null)
        {
            if (_occupationCheck.OccupyingPiece != null)
            {
                if (_occupationCheck.OccupyingPiece.CompareTag("WhitePiece"))
                {
                    _gm.WhitePlayerPieces.Remove(_occupationCheck.OccupyingPiece);
                }
                else if (_occupationCheck.OccupyingPiece.CompareTag("BlackPiece"))
                {
                    _gm.BlackPlayerPieces.Remove(_occupationCheck.OccupyingPiece);
                }

                Destroy(_occupationCheck.OccupyingPiece.gameObject);

                _occupationCheck.ResetOccupationSpecs();

                PieceDiceRoll(_moveCheck.ActivePiece);
            }

            _moveCheck.ActivePiece.position = new Vector3(transform.position.x, _moveCheck.ActivePiece.position.y, transform.position.z);
        }

        GetComponent<SpriteRenderer>().color = Color.blue;

        _moveCheck.ActivePiece = null;
        _moveCheck.ResetSquareHighlighters();

        _gm.SwitchTurn();
    }
#endif

    private void PieceDiceRoll(Transform activePiece)
    {
        int randomIndex = Random.Range(0, 10);
        GameObject instance = null;

        if (!activePiece.name.Contains("King"))
        {
            if (activePiece.CompareTag("WhitePiece"))
            {
                instance = Instantiate(_gm.WhitePieceType[randomIndex], activePiece.position, Quaternion.Euler(0f, 90f, 0f), _gm.WhitePlayer);
            }
            else if (activePiece.CompareTag("BlackPiece"))
            {
                instance = Instantiate(_gm.BlackPieceType[randomIndex], activePiece.position, Quaternion.Euler(0f, 90f, 0f), _gm.BlackPlayer);
            }

            if (_gm.WhitePlayerPieces.Contains(activePiece))
            {
                _gm.WhitePlayerPieces.Remove(activePiece);
                _gm.WhitePlayerPieces.Add(instance.transform);
            }

            if (_gm.BlackPlayerPieces.Contains(activePiece))
            {
                _gm.BlackPlayerPieces.Remove(activePiece);
                _gm.BlackPlayerPieces.Add(instance.transform);
            }

            Destroy(activePiece.gameObject);
            _moveCheck.ActivePiece = instance.transform;
        }
    }
}
