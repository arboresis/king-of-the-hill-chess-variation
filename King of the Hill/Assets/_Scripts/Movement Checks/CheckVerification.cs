﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckVerification : MonoBehaviour
{
    //Check if the opposite king can destroy this piece
    public bool KingCanDestroy = true;

    //The tag fragment which will be checked
    public string TagFragment;

    //This piece's movement directions
    protected List<Vector2> _Directions = new List<Vector2>();

    //Check if this piece should iterate
    protected bool _CheckShouldIterate;

    // Start is called before the first frame update
    void Awake()
    {
        //Instance creation
        _Directions = GetComponent<GenericPiece>().PieceDirections;
        _CheckShouldIterate = GetComponent<GenericPiece>().ShouldIterate;
        StartCoroutine(CustomUpdate());
    }

    private IEnumerator CustomUpdate()
    {
        while (isActiveAndEnabled)
        {
            yield return new WaitForSeconds(0.05f);
            
            /* STILL NOT WORKING
             * 
             * White Bishop covers White Tower,
             * Black Tower moves and destroys the White Bishop (resets the Tower's protection)
             * White Queen covers White Tower
             * White Tower's protection resets (even with the Black Tower in front)
             */

            if (GetComponent<GenericPiece>().Interactable)
            {
                KingCanDestroy = true;
            }
        }
    }

    protected void LateUpdate()
    {
        //KingCanDestroy = true;
        foreach (Vector2 direction in _Directions)
        {
            VerifyCheck(direction);
        }
    }

    protected void VerifyCheck(Vector2 direction)
    {
        RaycastHit hit;
        float rayLength = 1f;

        if (_CheckShouldIterate)
        {
            rayLength = 16f;
        }
        else
        {
            rayLength = direction.magnitude;
        }

        Physics.Raycast(transform.position, new Vector3(direction.x, 0f, direction.y), out hit, rayLength);
        Debug.DrawRay(transform.position, new Vector3(direction.x, 0f, direction.y), Color.grey);

        if (hit.collider != null)
        {
            if (hit.collider.CompareTag(tag))
            {
                hit.collider.GetComponent<CheckVerification>().KingCanDestroy = false;
            }

            if (hit.collider.name.Contains("King") && hit.collider.name.Contains(TagFragment))
            {
                Debug.Log("Check");
            }
        }
    }
}
