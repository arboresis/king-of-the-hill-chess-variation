﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCheck : MonoBehaviour
{
    //A list with all the squares in the board
    public List<Transform> BoardSquares = new List<Transform>();

    //The active piece
    public Transform ActivePiece;

    IEnumerator Start()
    {
        //Wait until the whole map is instanced
        yield return new WaitForEndOfFrame();

        //Add all of the white squares to the list
        foreach (GameObject whiteSquare in GameObject.FindGameObjectsWithTag("WhiteSquare"))
        {
            BoardSquares.Add(whiteSquare.transform);
        }
        //Add all of the black squares to the list
        foreach (GameObject blackSquare in GameObject.FindGameObjectsWithTag("BlackSquare"))
        {
            BoardSquares.Add(blackSquare.transform);
        }
    }

    public void ResetSquareHighlighters()
    {
        //Run through every square in the board
        foreach(Transform square in BoardSquares)
        {
            //Deactivate every square's availability highlight
            square.GetChild(0).gameObject.SetActive(false);
        }
    }

    public Transform GetSquare(Vector3 position)
    {
        //Instance the target square
        Transform targetSquare = null;

        //Check each square
        foreach(Transform square in BoardSquares)
        {
            //Check if this square's position is the same as the requested position
            if(square.position == position)
            {
                //The target square is this one
                targetSquare = square;
            }
        }

        //Return the target square
        return targetSquare;
    }

    public void LightUpSquare(Vector3 squarePosition)
    {
        //Run through every square in the board
        foreach (Transform square in BoardSquares)
        {
            //If the square's xx position is the same as the target square's xx position
            if (square.position.x == Mathf.RoundToInt(squarePosition.x))
            {
                //If the square's zz position is the same as the target square's zz position
                if (square.position.z == Mathf.RoundToInt(squarePosition.z))
                {
                    //Activate this square's availability highlight
                    square.GetChild(0).gameObject.SetActive(true);
                }
            }
        }
    }
}
