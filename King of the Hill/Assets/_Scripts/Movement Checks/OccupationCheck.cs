﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OccupationCheck : MonoBehaviour
{
    public bool
        //Checks if this square is occupied
        IsOccupied = false,
        //Checks if the piece occupying this square is white
        OccupyingPieceIsWhite = false;

    //The occupying piece
    public Transform OccupyingPiece;

    private void OnTriggerStay(Collider other)
    {
        //Set the square as occupied
        IsOccupied = true;

        //Set the piece that is occupying this space
        OccupyingPiece = other.transform;

        //Check if the piece occupying this square is white
        if (OccupyingPiece.CompareTag("WhitePiece"))
        {
            OccupyingPieceIsWhite = true;
        }
        //Check if the piece occupying this square is black
        else if (other.CompareTag("BlackPiece"))
        {
            OccupyingPieceIsWhite = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        ResetOccupationSpecs();
    }

    public void ResetOccupationSpecs()
    {
        //Reset the occupation specifications
        IsOccupied = false;
        OccupyingPieceIsWhite = false;
        OccupyingPiece = null;
    }
}
